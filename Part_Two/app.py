import yaml
from tinydb import TinyDB, Query
from utils.prompt_utility import display_chart, clear_terminal, user_prompt
import os
import sys
import signal

# Since this question is meant to have an initial qty's, we should delete db every run
if os.path.exists("db.json"):
    os.remove("db.json")

db = TinyDB('db.json')
initial_config = None

with open('part_two_values.yaml') as f:
    # load yaml config
    values = yaml.safe_load(f)
    initial_config = values


def initialize_db(initial_inventory):
    # Function to load initial values into the database for the beginning qtys of this task
    for item in initial_inventory:
        db.insert(item)


class InventoryStore():
    # Inventory Store accepts a TinyDB database with a single table with items that contain
    # keys:[id,item,qty] and values of type: [int,str,int].
    # Accepts a table_header list of strings that will describe the items in each column
    # of the database.
    def __init__(self, db, table_header=[]):
        self.db = db
        self.table_header = table_header

    def place_order(self, product_obj, qty):
        if product_obj:
            db_qty = product_obj.get('qty', 0)
            # Make sure there is enough inventory to place order
            if db_qty >= qty:
                new_qty = db_qty - qty
                db.update({'qty': new_qty}, Query().id ==
                          product_obj.get('id'))
                self.order_screen(msg="—— Your items have been ordered. ——")
            self.order_screen(
                msg="QTY must be greater or eqaul to current QTY!")

    def find_by_id(self, product_id):
        Item = Query()
        product = db.search(Item.id == product_id)
        if product:
            return product[0]
        return None

    def product_prompt(self):
        product_id, error = user_prompt("Please Enter The Product ID:")
        if error:
            self.order_screen(error)
        product = self.find_by_id(product_id)
        if product:
            return product
        self.order_screen(msg="Product ID must be valid!")

    def qty_prompt(self):
        qty, error = user_prompt("Please Enter QTY:")
        if error:
            self.order_screen(error)
        if qty > 0:
            return qty
        self.order_screen(
            msg="Qty must be a positive int greater than 0!")

    def order_screen(self, msg="Press Ctrl-c to Exit"):
        clear_terminal()
        print("Msg: {}".format(msg))
        # Order Items Values in a list of lists from the unordered dict.
        table = [[item.get('id'), item.get('item'), item.get('qty')]
                 for item in self.db.all()]
        display_chart(header=self.table_header, table=table)
        print("What would you like to order today ?")
        product = self.product_prompt()
        qty = self.qty_prompt()
        self.place_order(product, qty)

    def start(self):
        self.order_screen()


if __name__ == "__main__":
    # Handle errors from exit with a goodbye message
    try:
        # Load database with initial values for Part Two
        initialize_db(initial_config.get('initial_inventory'))

        # Look for table header list in config, defualt to empty iterable list
        table_header = initial_config.get('table_headers', [])
        Store = InventoryStore(
            db, table_header=table_header)
        Store.start()
    except:
        clear_terminal()
        print("Goodbye!")

from tabulate import tabulate
import os


def display_chart(table=[], header=[]):
    print(tabulate(table, header))


# Return User Input and Error String If Not Expected Input
def user_prompt(prompt="", expected_input=int):
    try:
        res = input('{}'.format(prompt))
        res = expected_input(res)
        return res, None
    except Exception as Error:
        return None, "User Must Enter A {}".format(str(expected_input))


def clear_terminal():
    os.system('cls' if os.name == 'nt' else 'clear')

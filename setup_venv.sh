#!/bin/sh

echo "-----Setting Up Virtual ENV-----"
virtualenv venv -p python3

echo "-----Activating Virtual ENV-----"
source venv/bin/activate
echo "-----Activated!-----"

echo 

echo "-----Installing Python Libraries-----"
pip install -r requirements.txt
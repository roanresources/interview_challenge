**Project Directory**

```bash
├── Part_One
│   ├── app.py
│   ├── base.py
│   ├── data.db
│   ├── sql_answers.yaml
│   └── tables
│       ├── __init__.py
│       ├── student_details.py
│       └── students_info.py
├── Part_Two
│   ├── app.py
│   ├── db.json
│   ├── initial_inventory.yaml
│   └── utils
│       ├── __init__.py
│       └── prompt_utility.py
├── README.md
├── requirements.txt
└── setup_venv.sh
```

---

## Setting Up Environment

**Dependencies and Requirements**

You will need Python3+ installed locally

**Setup**

In the Top Folder exectute the bash script **setup_venv.sh** in a Unix Terminal. This will create a virtual environment and install necessary libraries.

```bash
./setup_venv.sh
```

If it does not work try:

```bash
chmod +x setup_venv.sh
```

Activate the Virtual Env:

```bash
source venv/bin/activate
```

To Deactivate:

```bash
deactivate
```

---

## Part One

I created a python script that will create a local SQLite Database and execute raw SQL onto the tables from Part One. All SQL and Initial Table Values are stored in **Part_One/sql_answers.yaml**

Be sure that your virtual environment is activated and your terminal should have the prefix **(venv)**:

```bash
(venv) user@device:$
```

If not then execute the command described in the Setup Section above:

Execute app.py in the folder Part_One to test the SQL Statements.

```bash
cd Part_One
python app.py
```

1.  Display the names of all students who study Computer Science AND have a
    GPA of between 3.2 and 3.9


    **Answer:**
    "SELECT Name FROM students_info
    WHERE Course='Computer Science' AND GPA > 3.2 AND GPA < 3.9;"

2.  Display name of all students who have GPA of more than 3.0, excluding the
    ones who study Fashion.


    **Answer:**
    "SELECT Name FROM students_info
    WHERE GPA > 3.0 AND Course <> 'Fashion';"

3.  Show all details ( columns) for Students who are Male and study Computer
    Science


    **Answer:**
    "SELECT \* FROM students_info
    LEFT JOIN student_details ON students_info.Id=student_details.Id
    WHERE student_details.Sex='M' AND students_info.Course='Computer Science';"

4.  Display total count of male and female students who have GPA’s higher than
    3.4.


    **Answer:**
    "SELECT COUNT(Id) FROM students_info
    WHERE GPA > 3.4;"

5.  Display average age of all students enrolled in Computer Science.


    **Answer:**
    "SELECT AVG(Age)
    FROM student_details
    LEFT JOIN students_info ON student_details.Id=student_info.Id
    WHERE students_info.Course='Computer Science';"

6.  For each Course, display the count of students enrolled along with the course
    name.


    **Answer:**
    "SELECT Course, COUNT(Id)
    FROM students_info
    GROUP BY Course
    ORDER BY COUNT(Id) DESC;"

7.  List the student with the highest and lowest GPA. Display the name and their
    GPA.


    **Answer:**
    "SELECT Name, GPA
    FROM students_info
    WHERE GPA = (SELECT MAX(students_info.GPA) FROM students_info)
    OR GPA = (SELECT MIN(students_info.GPA) FROM students_info);"

---

## Part Two

Part Two is also initialized with a **yaml** file here: **Part_Two/part_two_values.yaml**. It utilizes an in-file NoSQL database called [TinyDB](https://tinydb.readthedocs.io/en/latest/getting-started.html).

Be sure that your virtual environment is activated and your terminal should have the prefix **(venv)**:

```bash
(venv) user@device:$
```

If not then execute the command described in Setup Section of the README above.

The app can be executed by the following command:

```bash
cd Part_Two
python app.py
```

To EXIT press Ctrl-c

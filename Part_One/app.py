# coding=utf-8
from datetime import datetime, timezone
from base import Session, engine, Base
from tables.student_details import StudentDetailsModel
from tables.students_info import StudentInfoModel
from tabulate import tabulate
import yaml
import os


sql_answers = {}
initial_students = {}

with open('sql_answers.yaml') as f:
    # load yaml config
    values = yaml.safe_load(f).get('part_one')
    sql_answers = values.get('sql_answers')
    initial_students = values.get('initial_students')

# Check if a previous sqlite file exists and if so delte it
if os.path.exists("data.db"):
    os.remove("data.db")

# Generate SQL schema
Base.metadata.create_all(engine)

# Create a new session
session = Session()


def create_entries():
    # creating a function specific for this task for creating the initial entries
    # outside of the potentially reusable Class Object
    for key, values in initial_students.items():
        # insert id(key) into values
        values['iD'] = key
        student_dict = values
        student_detail = StudentDetailsModel(**student_dict)
        student_info = StudentInfoModel(**student_dict)
        session.add(student_detail)
        session.add(student_info)
    session.commit()


class SqlExec():
    # Expects a Session to a db, a list of key: value("sql statement") dict objects
    # which will be executed and printed
    def __init__(self, db_session, sql_statements):
        self.db_session = db_session
        self.sql_statements = sql_statements

    def print_result(self, result):
        print('Results:')
        print(tabulate([list(row) for row in result]))
        print('\n')

    def execute_answers(self):
        for key in sorted(self.sql_statements):
            value = self.sql_statements.get(key)
            # Print the key value above SQL Result
            print("{}: \n{}\n".format(key, value))
            # Execute Value of sql_statement
            result = self.db_session.execute(value)
            self.print_result(result)

    def run(self):
        self.execute_answers()


if __name__ == '__main__':
    # Create initial database entries for the test
    create_entries()
    # Create and run the test by passing the sql_statements and db_session to SqlExec
    part_one = SqlExec(session, sql_answers)
    part_one.run()

from sqlalchemy import Column, String, Integer, Date, DateTime, Float
from datetime import datetime, timezone
from base import Base, Session


class StudentInfoModel(Base):
    __tablename__ = 'students_info'

    iD = Column('Id', Integer, primary_key=True)
    name = Column('Name', String(80))
    course = Column('Course', String(40))
    gpa = Column('GPA', Float)

    def __init__(self, iD=None, name=None, course=None, gpa=None, **data):
        self.iD = iD
        self.name = name
        self.course = course
        self.gpa = gpa

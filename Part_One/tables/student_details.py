from sqlalchemy import Column, String, Integer, Date, DateTime
from datetime import datetime, timezone
from base import Base, Session


class StudentDetailsModel(Base):
    __tablename__ = 'student_details'

    iD = Column('Id', Integer, primary_key=True)
    degree = Column('Degree', String(80))
    sex = Column('Sex', String(10))
    age = Column('Age', Integer)

    def __init__(self, iD=None, degree=None, sex=None, age=None, **data):
        self.iD = iD
        self.degree = degree
        self.sex = sex
        self.age = age
